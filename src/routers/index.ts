/**
 * Created by hakan on 04/07/2017.
 */

import {ProcessRoute} from './ProcessRoute';
import {TaskRoute} from './TaskRoute';

export default [
    ProcessRoute.create(),
    TaskRoute.create()
];
