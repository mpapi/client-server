/**
 * Created by hakan on 04/07/2017.
 */
import {Method} from '../../core/src/entity/routes/Method';
import {CoreRoutes} from '../../core/src/routers/CoreRoutes';
import {TaskController} from '../controllers/TaskController';
import {auth} from '../middleware/auth';

export class TaskRoute {

    public static create() {

        const authLayer = auth();

        return CoreRoutes.routes(
            TaskController,
            {
                fnName: 'run',
                routePath: '/task/run',
                method: Method.post,
                middleware: [authLayer]
            },
            {
                fnName: 'logs',
                routePath: '/task/logs',
                method: Method.post,
                middleware: [authLayer],
            }
        );
    }
}
