/**
 * Created by hakan on 04/07/2017.
 */
import {Method} from '../../core/src/entity/routes/Method';
import {CoreRoutes} from '../../core/src/routers/CoreRoutes';
import {ProcessController} from '../controllers/ProcessController';
import {auth} from '../middleware/auth';

export class ProcessRoute {

    public static create() {

        const authLayer = auth();

        return CoreRoutes.routes(
            ProcessController,
            {
                fnName: 'run',
                routePath: '/process/run',
                method: Method.post,
                middleware: [authLayer],
            }
        );
    }

}
