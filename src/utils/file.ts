import {path as rootPath} from 'app-root-path';
import fs from 'fs';
import mkdirp from 'mkdirp';
import path from 'path';
// @ts-ignore
import readLastLines from 'read-last-lines';

export const mkdir = (dir: string) => {
    return new Promise((resolve, reject) => {

        if (!fs.existsSync(dir)) {
            mkdirp(dir, (err) => {
                if (err) {
                    reject(err);
                } else {
                    resolve();
                }
            });
        } else {
            resolve();
        }
    });
};

export const write = async (file: string, text: string) => {
    return new Promise((resolve, reject) => {
        fs.writeFile(file, text, err => {
            if (err) {
                reject(err);
            } else {
                resolve();
            }
        });
    });
};

export const unlink = async (file: string) => {
    return new Promise((resolve, reject) => {
        fs.unlink(file, (err) => {
            if (err) {
                reject(err);
            } else {
                resolve();
            }
        });
    });
};

export const append = async (file: string, text: string) => {
    return new Promise((resolve, reject) => {
        fs.appendFile(file, text, (err) => {
            if (err) {
                reject(err);
            } else {
                resolve();
            }
        });
    });
};

export const readLines = async (file: string, limit: number = 100) => {

    if (fs.existsSync(file)) {
        const lineText = await readLastLines.read(file, limit);
        return lineText.split('\n');
    }

    return [];
};

export const pathResolve = (...paths: string[]): string => {
    const file = path.join(...paths);
    return path.resolve(path.normalize(file));
};
export const rootPathResolve = (...paths: string[]): string => {
    const file = path.join(rootPath, ...paths);
    return path.resolve(path.normalize(file));
};
