import {NextFunction, Request, Response} from 'express';

import {ErrorModel} from '../../core/src/models/ErrorModel';
import {Api} from '../remote/Api';

export const auth = () => async (req: Request, res: Response, next: NextFunction) => {
    const token = req.headers && req.headers && req.headers.token;

    if (token) {
        const result: any = await Api.request('/user/auth', {}, token.toString());

        req.headers.user = result.user;
        next();
    } else {

        throw new ErrorModel('TOKEN_NOT_FOUND', 200);
    }
};
