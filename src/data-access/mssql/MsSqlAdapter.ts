/**
 * Created by hakan on 11/07/2018.
 */
import * as sql from 'mssql';

import {ErrorModel} from '../../../core/src/models/ErrorModel';
import {DataAdapterCore} from '../DataAdapterCore';
import {IDataAdapter} from '../IDataAdapter';

export class MsSqlAdapter extends DataAdapterCore implements IDataAdapter {

    public db: sql.ConnectionPool | null = null;
    public transaction: sql.Transaction | null = null;

    public prepare(request: sql.Request, command: string, parameters: any): any {

        const result: { command: string, request: sql.Request, errorText: string | boolean } = {
            command: (command || '').trim(),
            request,
            errorText: this.getErrorText(command, parameters),
        };

        if (result.errorText !== false) {
            return result;
        }

        const keys = this.getMatchs(result.command);
        result.command = result.command.replace(/\$\$/g, '@');

        for (const name of keys) {

            const value = parameters[name];

            result.request.input(name, value);
        }

        return result;
    }

    public connect(): Promise<any> {

        return new Promise<any>((resolve, reject) => {
            const cfg = {
                user: this.dbcfg.user,
                password: this.dbcfg.password,
                server: this.dbcfg.host || 'localhost',
                database: this.dbcfg.database,
                // pool: {
                //     max: 1,
                //     min: 0,
                //     idleTimeoutMillis: 30000
                // }
                options: {encrypt: false},
            };

            this.db = new sql.ConnectionPool(cfg, (err: any) => {
                if (err) {
                    reject(new ErrorModel('MSSQL_CONNECTION_ERROR', 200, err));
                } else {
                    this.startTransaction().then(resolve, reject);
                }
            });
        });
    }

    public startTransaction(): Promise<any> {

        return new Promise<any>((resolve, reject) => {
            if (this.db == null) {
                reject(new ErrorModel('MSSQL_DB_INIT_ERROR', 200));
                return;
            }

            this.transaction = new sql.Transaction(this.db);
            this.transaction.begin(sql.ISOLATION_LEVEL.SERIALIZABLE, (err: any) => {
                if (err) {
                    reject(new ErrorModel('MSSQL_START_TRANSACTION_ERROR', 200, err));
                } else {
                    resolve();
                }
            });
        });
    }

    public execute(queryItem: any, parameters: any): Promise<any> {

        return new Promise<any>((resolve, reject) => {
            if (this.transaction == null) {
                reject(new ErrorModel('MSSQL_TRANSITION_INIT_ERROR', 200));
                return;
            }

            const request = new sql.Request(this.transaction);

            const prepared = this.prepare(request, queryItem.command, parameters);
            if (prepared.errorText !== false) {
                reject(new ErrorModel(prepared.errorText, 200));
                return;
            }
            prepared.request.query(prepared.command, (err: any, result: any) => {
                if (err) {
                    reject(new ErrorModel('TRANSACTION_QUERY_ERROR', 200, err));
                } else {
                    resolve(result.recordset);
                }
            });
        });
    }

    public disconnect(error?: boolean): Promise<any> {

        return new Promise<any>((resolve, reject) => {
            if (this.transaction == null) {
                reject(new ErrorModel('MSSQL_TRANSITION_INIT_ERROR', 200));
                return;
            }

            this.transaction.commit((err: any) => {
                if (this.db == null) {
                    reject(new ErrorModel('MSSQL_DB_INIT_ERROR', 200));
                    return;
                }

                if (this.transaction == null) {
                    reject(new ErrorModel('MSSQL_TRANSITION_INIT_ERROR', 200));
                    return;
                }

                if (error || err) {
                    this.transaction.rollback(() => {
                        if (this.db == null) {
                            reject(new ErrorModel('MSSQL_DB_INIT_ERROR', 200));
                            return;
                        }

                        this.db.close(() => {
                            reject(new ErrorModel('TRANSACTION_COMMIT_ERROR', 200, error || err));
                        });
                    });
                } else {
                    this.db.close(() => {
                        resolve();
                    });
                }
            });
        });
    }
}
