/**
 * Created by hakan on 11/07/2018.
 */

export interface IDataAdapter {

    connect(): Promise<any>;

    execute(queryItem: any, parameters: any): Promise<any>;

    disconnect(error?: any): Promise<any>;

}
