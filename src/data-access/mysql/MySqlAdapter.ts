/**
 * Created by hakan on 11/07/2018.
 */
import {Connection, createConnection, escape, MysqlError} from 'mysql';

import {ErrorModel} from '../../../core/src/models/ErrorModel';
import {QueryEntity} from '../../entity/QueryEntity';
import {DataAdapterCore} from '../DataAdapterCore';
import {IDataAdapter} from '../IDataAdapter';

export class MySqlAdapter extends DataAdapterCore implements IDataAdapter {

    public db: Connection | null = null;

    private prepareFormatter(): any {
        if (this.db == null) {
            throw new ErrorModel('MYSQL_DB_INIT_ERROR', 200);
        }

        this.db.config.queryFormat = (query, values) => {
            if (!values) {
                return query;
            }

            return query.replace(/\$\$(\w+)/g, (txt, key) => {

                if (values.hasOwnProperty(key)) {
                    return escape(values[key]);
                }
                return txt;
            });
        };
    }

    public connect(): Promise<any> {

        return new Promise<any>((resolve, reject) => {
            const cfg = {
                host: this.dbcfg.host || 'localhost',
                user: this.dbcfg.user,
                password: this.dbcfg.password,
                database: this.dbcfg.database,
                port: this.dbcfg.port || 3306
            };

            this.db = createConnection(cfg);

            this.db.connect((err) => {
                if (err) {
                    reject(new ErrorModel('MYSQL_CONNECTION_ERROR', 200, err));
                } else {
                    this.prepareFormatter();
                    this.startTransaction().then(resolve, reject);
                }
            });
        });
    }

    public startTransaction(): Promise<any> {

        return new Promise<any>((resolve, reject) => {
            if (this.db == null) {
                reject(new ErrorModel('MYSQL_DB_INIT_ERROR', 200));
                return;
            }

            this.db.beginTransaction((err: MysqlError) => {
                if (err) {
                    reject(new ErrorModel('MYSQL_START_TRANSACTION_ERROR', 200, err));
                } else {
                    resolve();
                }
            });
        });
    }

    public execute(queryItem: QueryEntity, parameters: any): Promise<any> {

        return new Promise<any>((resolve, reject) => {
            if (this.db == null) {
                reject(new ErrorModel('MYSQL_DB_INIT_ERROR', 200));
                return;
            }

            const errorText = this.getErrorText(queryItem.command, parameters);

            if (errorText !== false || !queryItem.command) {
                reject(new ErrorModel(errorText || 'UNDEFINED_COMMAND', 200));
                return;
            }

            this.db.query(queryItem.command, parameters, (error, results) => {
                if (error) {
                    reject(new ErrorModel('TRANSACTION_QUERY_ERROR', 200, error));
                }

                resolve(results);
            });
        });
    }

    private rollback(err: any) {
        return new Promise<any>((resolve, reject) => {

            if (this.db == null) {
                throw new ErrorModel('MYSQL_DB_INIT_ERROR', 200);
            }

            const e = new ErrorModel('TRANSACTION_COMMIT_ERROR', 200, JSON.stringify(err));

            this.db.rollback(() => reject(e));
        });
    }

    public disconnect(error?: any): Promise<any> {

        return new Promise<any>((resolve, reject) => {

            if (this.db == null) {
                reject(new ErrorModel('MYSQL_DB_INIT_ERROR', 200));
                return;
            }

            this.db.commit((err: MysqlError) => {

                if (error || err) {

                    this.rollback(error || err).then(resolve, reject);

                } else {

                    if (this.db != null) {
                        this.db.end();
                    }

                    resolve();
                }
            });

        });
    }
}
