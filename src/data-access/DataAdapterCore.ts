import * as _ from 'lodash';

/**
 * Created by hakan on 11/07/2018.
 */

export class DataAdapterCore {

    protected transaction: any;
    protected db: any;

    public constructor(protected dbcfg: any) {
    }

    protected getMatchs(command: string): string[] {
        const matchs = command.match(/\$\$\w+/g);
        if (matchs != null) {
            for (let i = 0; i < matchs.length; i++) {
                matchs[i] = matchs[i].replace('$$', '');
            }
            return matchs;
        }
        return [];
    }

    protected getMatchsUniq(command: string): string[] {
        const matchs = command.match(/\$\$\w+/g);
        if (matchs != null) {
            for (let i = 0; i < matchs.length; i++) {
                matchs[i] = matchs[i].replace('$$', '');
            }
            return _.sortedUniq(matchs);
        }
        return [];
    }

    protected getErrorText(command: string | undefined, parameters: any): string | boolean {
        if (!command) {
            return 'UNDEFINED_COMMAND';
        }

        const matchs = this.getMatchsUniq(command);

        const notFounds: string[] = [];

        for (const match of matchs) {

            if (_.isUndefined(parameters[match]) && _.indexOf(notFounds, match) === -1) {
                notFounds.push(match);
            }
        }
        if (notFounds.length > 0) {
            return 'PARAMETERS_NOT_FOUND_(' + notFounds.join(',') + ')';
        }

        return false;
    }
}
