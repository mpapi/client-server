/**
 * Created by hakan on 11/07/2018.
 */
import fs from 'fs';
import * as _ from 'lodash';

import {ErrorModel} from '../../../core/src/models/ErrorModel';
import {pathResolve, rootPathResolve} from '../../utils/file';
import {DataAdapterCore} from '../DataAdapterCore';
import {IDataAdapter} from '../IDataAdapter';

export class FileAdapter extends DataAdapterCore implements IDataAdapter {

    dataDir = rootPathResolve(this.dbcfg.dir || '../datas');

    private isDirectory(): Promise<boolean> {
        return new Promise<boolean>((resolve, reject) => {

            fs.stat(this.dataDir, (err, stat) => {

                if (err || !stat.isDirectory()) {

                    reject(new ErrorModel('JSON_FOLDER_NOT_EXISTS', 200));
                } else {

                    resolve();
                }
            });
        });
    }

    public connect(): Promise<any> {
        return this.isDirectory();
    }

    public execute(queryItem: any, parameters: any): Promise<any> {
        return new Promise<any>((resolve, reject) => {

            const file = pathResolve(this.dataDir, queryItem.command);

            if (!fs.existsSync(file)) {
                reject(new ErrorModel('JSON_FILE_NOT_FOUND_ERROR', 200));
                return;
            }

            fs.readFile(file, 'utf8', (err, data) => {
                if (err) {
                    throw err;
                }
                try {
                    const result = JSON.parse(data);

                    if (_.isArray(result)) {

                        resolve(result);

                    } else {

                        reject(new ErrorModel('UNSUPPORTED_JSON_FORMAT', 200));
                    }

                } catch (e) {
                    reject(new ErrorModel('JSON_PARSE_ERROR', 200));
                }
            });
        });
    }

    async disconnect() {
    }

}
