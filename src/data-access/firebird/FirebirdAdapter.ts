/**
 * Created by hakan on 11/07/2018.
 */
import * as _ from 'lodash';
import * as Firebird from 'node-firebird';

import {ErrorModel} from '../../../core/src/models/ErrorModel';
import {DataAdapterCore} from '../DataAdapterCore';
import {IDataAdapter} from '../IDataAdapter';

export class FirebirdAdapter extends DataAdapterCore implements IDataAdapter {

    public db: Firebird.Database | null = null;
    public transaction: Firebird.Transaction | null = null;

    public replaceCmd(cmd: string, searchValue: string, replaceValue?: any): string | boolean {
        if (_.isUndefined(replaceValue)) {
            replaceValue = '?';
        }

        if (cmd.indexOf(searchValue) !== -1) {
            return cmd.replace(searchValue, replaceValue);
        }
        return false;
    }

    public prepare(command: string, parameters: any) {

        const result: any = {
            command: (command || '').trim(),
            parameters: [],
            errorText: this.getErrorText(command, parameters),
        };

        if (result.errorText !== false) {
            return result;
        }

        const keys = this.getMatchs(result.command);

        for (const name of keys) {

            const value = parameters[name];

            const preparedValue = _.isString(value) ? Firebird.escape(value) : value;

            result.command = this.replaceCmd(result.command, '$$' + name, preparedValue);
        }

        return result;
    }

    public connect(): Promise<any> {

        return new Promise<any>((resolve, reject) => {
            const opts = {
                host: this.dbcfg.host || 'localhost',
                port: this.dbcfg.port || 3050,
                database: this.dbcfg.database,
                user: this.dbcfg.user || 'sysdba',
                password: this.dbcfg.password || 'masterkey',
            };

            Firebird.attach(opts, (err, db) => {
                if (err) {

                    console.log('FIREBIRD_CONNECTION_ERROR:', err.message, err.stack);

                    reject(new ErrorModel('FIREBIRD_CONNECTION_ERROR', 200, err));
                } else {
                    this.db = db;
                    this.startTransaction().then(resolve, reject);
                }
            });
        });
    }

    public startTransaction(): Promise<any> {

        return new Promise<any>((resolve, reject) => {
            if (this.db == null) {
                reject(new ErrorModel('FIREBIRD_INIT_ERROR', 200));
                return;
            }

            this.db.transaction(Firebird.ISOLATION_SERIALIZABLE, (err, transaction) => {
                if (err) {
                    reject(new ErrorModel('FIREBIRD_START_TRANSACTION_ERROR', 200, err));
                } else {
                    this.transaction = transaction;
                    resolve();
                }
            });
        });
    }

    public execute(queryItem: any, parameters: any): Promise<any> {

        return new Promise<any>((resolve, reject) => {
            const prepared = this.prepare(queryItem.command, parameters);

            if (prepared.errorText !== false) {
                reject(new ErrorModel(prepared.errorText, 200));
                return;
            }

            if (this.transaction == null) {
                reject(new ErrorModel('FIREBIRD_TRANSITION_INIT_ERROR', 200));
                return;
            }

            this.transaction.query(prepared.command, prepared.parameters, (err: any, result: any[]) => {
                if (err) {
                    reject(new ErrorModel('TRANSACTION_QUERY_ERROR', 200, err && err.message));
                } else {
                    resolve(result);
                }
            });
        });
    }

    public disconnect(error?: any): Promise<any> {

        return new Promise<any>((resolve, reject) => {
            if (this.transaction == null) {
                reject(new ErrorModel('FIREBIRD_TRANSITION_INIT_ERROR', 200));
                return;
            }

            this.transaction.commit((err) => {
                if (this.db == null) {
                    reject(new ErrorModel('FIREBIRD_INIT_ERROR', 200));
                    return;
                }

                if (this.transaction == null) {
                    reject(new ErrorModel('FIREBIRD_TRANSITION_INIT_ERROR', 200));
                    return;
                }

                if (error || err) {
                    this.transaction.rollback();
                    reject(new ErrorModel('TRANSACTION_COMMIT_ERROR', 200, error || err));
                } else {
                    this.db.detach();
                    resolve();
                }
            });
        });
    }
}
