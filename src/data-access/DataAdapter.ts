/**
 * Created by hakan on 11/07/2018.
 */
import * as _ from 'lodash';

import {ErrorModel} from '../../core/src/models/ErrorModel';
import {Config} from '../../core/src/utils/Config';

import {FileAdapter} from './file/FileAdapter';
import {FirebirdAdapter} from './firebird/FirebirdAdapter';
import {IDataAdapter} from './IDataAdapter';
import {MsSqlAdapter} from './mssql/MsSqlAdapter';
import {MySqlAdapter} from './mysql/MySqlAdapter';

const dbList = Config.get('dbList');

export class DataAdapter {

    public constructor(protected process: any, protected parameters: any[], protected defaults: any) {
        this.initPreParams();
        this.initPostParams();
    }

    private adapter: IDataAdapter | null = null;
    private beforeResult: any;
    private result: any;

    public initPreParams() {

        const pre = _.filter(this.process.queryItems, (queryItem: any) => {
            return _.startsWith(queryItem.queryKey, 'pre');
        });
        const preParams: any[] = [];
        pre.forEach((preQuery: any) => {
            preParams.push({queryKey: preQuery.queryKey, values: _.assign({}, this.defaults)});
        });
        this.parameters = [...preParams, ...this.parameters];
    }

    public initPostParams() {

        const post = _.filter(this.process.queryItems, (queryItem: any) => {
            return _.startsWith(queryItem.queryKey, 'post');
        });

        const postParams: any[] = [];

        post.forEach((postQuery: any) => {
            postParams.push({queryKey: postQuery.queryKey, values: _.assign({}, this.defaults)});
        });

        this.parameters = [...this.parameters, ...postParams];
    }

    public async run(): Promise<any> {
        this.checkQueries();

        const dbcfg = this.getDbConfig(this.process.dbKey);

        if (dbcfg == null) {
            throw new ErrorModel('DB_CONFIG_NOT_FOUND', 200);
        }

        this.adapter = this.getDbAdapter(dbcfg);

        if (this.adapter == null) {
            throw new ErrorModel((dbcfg.type.toUpperCase()) + '_ADAPTER_NOT_FOUND', 200, 200);
        }

        await this.adapter.connect();

        try {

            for (let i = 0; i < this.parameters.length; i++) {
                const queryParameter = this.parameters[i];

                const queryKey = queryParameter.queryKey || 'main';

                const queryItem = this.getQueryItem(queryKey);

                if (queryItem == null) {
                    throw new ErrorModel('QUERY_ITEM_NOT_FOUND_(' + queryKey + ')', 200);
                }

                const parameters = _.assign(this.beforeResult || {}, this.defaults, queryParameter.values);

                const result = await this.adapter.execute(queryItem, parameters);

                // main sorgusu ise veriler this.result içerisine alınır.
                // veya döngü sorgusu değise ve son query ise result içerisine alınır.
                if (queryKey === 'main' || (i === this.parameters.length - 1 && queryItem.loopQuery === false)) {
                    this.result = _.cloneDeep(result);
                }

                // döngü sorugus değilse alınan mevcut veriyi beforeResult içerisine ekler
                // ve sonraki sorgularda kullanılabilir hale getirir.
                if (!queryItem.loopQuery && !_.isUndefined(result)) {

                    this.beforeResult = result.length > 0 ? _.cloneDeep(result[0]) : {};
                }
            }

        } catch (error) {

            await this.adapter.disconnect(error);

            throw error;
        }

        await this.adapter.disconnect();

        return this.result;
    }

    protected getQueryItem(queryKey: string) {

        for (const queryItem of this.process.queryItems) {

            if (queryItem.queryKey === queryKey) {
                return queryItem;
            }
        }
        return null;
    }

    protected getDbConfig(dbKey: string): any | null {

        for (const item of dbList) {

            if (dbKey === item.key) {

                return item;
            }
        }

        return null;
    }

    protected checkQueries(): string | null {

        const notFounds = [];

        for (const queryParameter of this.parameters) {

            const queryKey = queryParameter.queryKey || 'main';

            if (notFounds.indexOf(queryKey) === -1 && this.getQueryItem(queryKey) == null) {
                notFounds.push(queryKey);
            }
        }

        if (notFounds.length > 0) {

            throw new ErrorModel('QUERY_KEYS_NOT_FOUND_(' + notFounds.join(',') + ')', 200);
        }
        return null;
    }

    protected getDbAdapter(dbcfg: any): IDataAdapter | null {

        let adapter: IDataAdapter;

        switch (dbcfg.type) {
            case 'firebird':
                adapter = new FirebirdAdapter(dbcfg);
                break;
            case 'mssql':
                adapter = new MsSqlAdapter(dbcfg);
                break;
            case 'mysql':
                adapter = new MySqlAdapter(dbcfg);
                break;
            case 'json':
                adapter = new FileAdapter(dbcfg);
                break;
            default:
                return null;
        }

        return adapter;
    }

}
