/**
 * Created by hakan on 04/07/2017.
 */
import {HttpController} from '../../core/src/controllers/HttpController';
import {Api} from '../remote/Api';

export class BaseController extends HttpController {

    protected async api(path: string, data: any = {}): Promise<any> {
        return Api.request(path, data, this.getToken());
    }
}
