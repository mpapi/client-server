/**
 * Created by hakan on 04/07/2017.
 */
import * as _ from 'lodash';

import {ErrorModel} from '../../core/src/models/ErrorModel';
import {DataAdapter} from '../data-access/DataAdapter';

import {BaseController} from './BaseController';

export class ProcessController extends BaseController {

    /**
     * @apiVersion 1.0.0
     * @api {post} /process/run İşlemi Çalıştır
     * @apiName ProcessRun
     * @apiGroup DataAdapter
     * @apiDescription İşlemi çalıştırır
     *
     * @apiHeader {String} token Kullanıcı Erişim Anahtarı. Login Sırasında verilir.
     *
     * @apiParam {String} processKey İşlemin ID'si
     * @apiParam {json} parameters İşlemi çalıştırmak için gerekli olabilecek parametreleri içerir.
     * @apiParam {json} defaults kullanıcının varsayılan parametrelerini içerir.
     *
     * @apiSuccess {String} success Bilgi.
     *
     * @apiSuccessExample {json} Success-Response:
     *     HTTP/1.1 200 OK
     *      {
     *          "items": []
     *      }
     *
     * @apiError {String} error Hata Bilgisini İçerir
     *
     * @apiSampleRequest /process/run
     */
    public async run() {

        this.requireParameters(['processKey', 'parameters']);

        const processKey = this.getParam('processKey');
        const parameters = this.getParam('parameters');
        const defaults = this.getParam('defaults');

        // request main server. getting the process item
        const result = await this.api('/process/get', {processKey});

        const processItem = result.item;

        if (_.isUndefined(processItem) || processItem == null) {
            throw new ErrorModel('PROCESS_NOT_FOUND', 200);
        }

        const adapter: DataAdapter = new DataAdapter(processItem, parameters, defaults);

        const dataResult = await adapter.run();

        this.response.send({items: dataResult || []});

    }
}
