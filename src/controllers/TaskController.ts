/**
 * Created by hakan on 04/07/2017.
 */
import * as _ from 'lodash';

import {ErrorModel} from '../../core/src/models/ErrorModel';
import {Api} from '../remote/Api';
import Tasks from '../tasks';

import {BaseController} from './BaseController';

export class TaskController extends BaseController {

    /**
     * @apiVersion 1.0.0
     * @api {post} /task/run Görevi Çalıştır
     * @apiName TaskRun
     * @apiGroup Tasks
     * @apiDescription Görevi çalıştırır
     *
     * @apiHeader {String} token Kullanıcı Erişim Anahtarı. Login Sırasında verilir.
     *
     * @apiParam {String} taskKey Görev Key'i
     *
     * @apiSuccess {String} success Bilgi.
     *
     * @apiSuccessExample {json} Success-Response:
     *     HTTP/1.1 200 OK
     *      {
     *          "success": 'OK'
     *      }
     *
     * @apiError {String} error Hata Bilgisini İçerir
     *
     * @apiSampleRequest /task/run
     */
    public async run() {

        this.requireParameters(['taskKey']);

        const key = this.getParam('taskKey');

        // request main server. getting the process item
        const result: any = await Api.request('/user/tasks');
        const task = _.find<any>(result.items, {key});

        if (_.isNil(task)) {
            throw new ErrorModel('TASK_NOT_FOUND', 200);
        } else {

            // skip await
            Tasks.taskStart(task);

            this.response.send({success: 'OK'});

        }

    }

    /**
     * @apiVersion 1.0.0
     * @api {post} /task/logs Görev Logları
     * @apiName TaskRun
     * @apiGroup Tasks
     * @apiDescription Görevi loglarını gönderir.
     *
     * @apiHeader {String} token Kullanıcı Erişim Anahtarı. Login Sırasında verilir.
     *
     * @apiParam {String} taskKey Görev Key'i
     *
     * @apiSuccess {String} success Bilgi.
     *
     * @apiSuccessExample {json} Success-Response:
     *     HTTP/1.1 200 OK
     *      {
     *          "success": 'OK'
     *      }
     *
     * @apiError {String} error Hata Bilgisini İçerir
     *
     * @apiSampleRequest /task/logs
     */
    public async logs() {

        this.requireParameters(['taskKey']);

        const key = this.getParam('taskKey');

        // request main server. getting the process item
        const result: any = await Api.request('/user/tasks');
        const task = _.find<any>(result.items, {key});

        if (_.isNil(task)) {

            throw new ErrorModel('TASK_NOT_FOUND', 200);

        } else {

            const logs = await Tasks.taskLogs(task);

            this.response.send({items: logs});
        }

    }
}
