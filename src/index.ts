/**
 * Created by hakan on 04/07/2017.
 */
import Application from '../core/src/server';

import Routers from './routers';
import Tasks from './tasks';

class App extends Application {

    async afterConfigHooks(): Promise<void> {
        Tasks.register();
    }

    async registerRoutes(): Promise<any[]> {
        return Routers;
    }

}

(async () => {

    const app = new App();
    await app.listen();

})();
