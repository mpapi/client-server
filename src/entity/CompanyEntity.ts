export class CompanyEntity {
    _id?: string;
    title?: string;
    createdTime?: string;
    expiredTime?: string;
    userLimit?: number;
    verified?: boolean;
    active?: boolean;
    added?: {
        userId?: string,
        datetime?: string;
    };
    deleted?: boolean;
}
