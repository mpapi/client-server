import {MpChartEntity} from './MpChartEntity';
import {MpDashboardEntity} from './MpDashboardEntity';
import {MpPresentParamEntity} from './MpPresentParamEntity';

export class MpAnalysisEntity {
    presentParams?: MpPresentParamEntity[];
    dashboard?: MpDashboardEntity[];
    charts?: MpChartEntity[];
}
