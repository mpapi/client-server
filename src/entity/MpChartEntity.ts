export class MpChartEntity {
    title?: string;
    subtitle?: string;
    icon?: string;
    labelColumnName?: string;
    xLabel?: string;
    yLabel?: string;
    gridColor?: string;
    domainPadding?: number;
    common?: {
        axis?: string,
        axisExample?: string
        columnName?: string;
    };
    dataConfigs?: Array<{
        type?: string;
        text?: string;
        columnName?: string;
        color?: string;
    }>;
}
