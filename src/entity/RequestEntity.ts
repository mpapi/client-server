export class RequestEntity {

    processKey?: string;
    defaults?: any;
    parameters?: Array<{
        queryKey?: string,
        values?: any
    }>;

}
