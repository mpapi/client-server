import {ActionEntity} from './ActionEntity';
import {CompanyEntity} from './CompanyEntity';
import {DbEntity} from './DbEntity';
import {DefaultEntity} from './DefaultEntity';
import {PermissionEntity} from './PermissionEntity';
import {ServerEntity} from './ServerEntity';
import {UserDetailEntity} from './UserDetailEntity';

export class UserEntity {
    _id?: string;
    companyId?: string;
    company?: CompanyEntity;
    fullname?: string;
    email?: string;
    username?: string;
    password?: string;
    details?: UserDetailEntity[];
    expiredTime?: string;
    verified?: boolean;
    active?: boolean;
    added?: ActionEntity;
    adminCompanies?: string[];
    servers?: ServerEntity[];
    dbList?: DbEntity[];
    defaults?: DefaultEntity[];
    roles?: string[];
    permission?: PermissionEntity;
    deleted?: ActionEntity | false;
}
