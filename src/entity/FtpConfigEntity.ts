export class FtpConfigEntity {
    key?: string;
    host?: string;
    user?: string;
    password?: string;
    port?: number;
    dest?: string;
}
