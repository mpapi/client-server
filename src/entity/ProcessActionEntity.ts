export class ProcessActionEntity {
    title?: string;
    default?: boolean;
    icon?: string;
    queryKey?: string;
    params?: any;
}
