import {ProcessActionEntity} from './ProcessActionEntity';
import {QueryEntity} from './QueryEntity';

export class ProcessEntity {
    _id?: string;
    categoryId?: string;
    categoryTitle?: string;
    title?: string;
    processKey?: string;
    systemType?: string;
    expiredTime?: string;
    dbKey?: string;
    icon?: string;
    queryItems?: QueryEntity[];
    actions?: ProcessActionEntity[];
}
