import AdmZip from 'adm-zip';
import {Client} from 'basic-ftp';
import fs from 'fs';
import * as _ from 'lodash';
import * as schedule from 'node-schedule';

import {ErrorModel} from '../../core/src/models/ErrorModel';
import {Config} from '../../core/src/utils/Config';
import {DataAdapter} from '../data-access/DataAdapter';
import {FtpConfigEntity} from '../entity/FtpConfigEntity';
import {TaskEntity} from '../entity/TaskEntity';
import {UploaderTaskEntity} from '../entity/UploaderTaskEntity';
import {Api} from '../remote/Api';
import {mkdir, pathResolve, rootPathResolve, unlink, write} from '../utils/file';

import {BaseTask} from './BaseTask';

export class UploaderTask extends BaseTask<UploaderTaskEntity> {

    private readonly taskOutput: string;
    private readonly zipFile: string;
    private readonly ftpConfig: FtpConfigEntity;
    private readonly directory: string;
    private readonly cron: string;
    private readonly uploadPath: string;

    private results: any = {};

    constructor(task: TaskEntity<UploaderTaskEntity>) {
        super(task);

        this.taskOutput = rootPathResolve(Config.get('task.output'));
        this.zipFile = pathResolve(this.taskOutput, 'results.zip');
        this.ftpConfig = Config.find<FtpConfigEntity>('ftpList', {key: (task.options && task.options.ftpKey) || 'default-ftp'});
        this.uploadPath = pathResolve(this.ftpConfig.dest || '/', 'results.zip');
        this.directory = rootPathResolve(this.task.options.folder || './tasks');
        this.cron = (this.task.options && this.task.options.cron) || '';

        this.createDirectories();
    }

    public async schedule(): Promise<void> {
        schedule.scheduleJob(this.jobName, this.cron, this.start);
    }

    public cancel(): void {
        schedule.cancelJob(this.jobName);
    }

    private async createDirectories() {
        await mkdir(this.directory);
        await mkdir(this.taskOutput);
    }

    private async write() {
        const file = pathResolve(this.directory, 'results.json');
        const text = JSON.stringify(this.results);
        return write(file, text);
    }

    private async generateZip() {
        return new Promise((resolve, reject) => {

            const zip = new AdmZip();

            zip.addLocalFolder(this.directory, '/');

            zip.writeZip(this.zipFile, err => {
                if (err) {
                    reject(err);
                } else {
                    resolve();
                }
            });
        });
    }

    private async removeFiles() {
        await unlink(this.zipFile);
    }

    private async uploadFiles() {
        if (!this.ftpConfig) {
            throw new Error('FtpConfig not Found');
        }

        const {host, port, user, password} = this.ftpConfig;
        const client = new Client();

        await client.access({
            host,
            port: port || 21,
            user,
            password
        });

        await client.upload(fs.createReadStream(this.zipFile), this.uploadPath);
    }

    public async start() {
        try {
            await this.startTask();
        } catch (e) {
            console.error(e);
            await this.flog('error->', e.error || e.message);
        }
    }

    private async startTask() {
        if (!this.task.data) {
            throw new ErrorModel('DATA_NOT_FOUND', 200);
        }

        await this.createDirectories();

        this.results = {};

        for (const item of this.task.data) {

            if (_.isNil(item.processKey)) {
                throw new ErrorModel('PROCESS_KEY_NOT_FOUND', 200);
            }

            if (_.isUndefined(item.parameters) || item.parameters == null || item.parameters.length === 0) {
                throw new ErrorModel('PARAMETERS_NOT_FOUND', 200);
            }

            const data = {
                userId: this.task.userId,
                processKey: item.processKey
            };

            await this.flog(`İşlem Ayarları Alınıyor... -> İşlem Anahtarı: ${data.processKey}`);
            // request main server. getting the process item
            const result: any = await Api.request('/process/get', data);

            const processItem = result.item;

            if (_.isUndefined(processItem) || processItem == null) {
                throw new ErrorModel('PROCESS_NOT_FOUND', 200);
            }

            const adapter: DataAdapter = new DataAdapter(processItem, item.parameters, item.defaults || {});

            await this.flog(`İşlem çalıştırılıyor...`);
            this.results[processItem.processKey] = await adapter.run();

            await this.flog(`İşlem çalıştırıldı.`);
        }
        // loop end

        // save files
        await this.write();
        await this.flog(`İşlem sonuçları kaydedildi.`);

        // generate zip
        await this.generateZip();

        // connect ftp
        await this.uploadFiles();
        await this.flog(`Zip Dosyası Upload edildi.`);

        await this.removeFiles();
        await this.flog(`Upload edilen dosyalar kaldırıldı.`);

        // TODO notify mail|sms|push
        console.log(`notify request hazırlanacak.`);

        // success
        await this.flog(`Görev Tamamlandı`);
    }

}
