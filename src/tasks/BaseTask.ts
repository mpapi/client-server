import * as fs from 'fs';
import _ from 'lodash';
import moment from 'moment';
import path from 'path';

import {CoreTask} from '../../core/src/tasks/CoreTask';
import {Config} from '../../core/src/utils/Config';
import {TaskEntity} from '../entity/TaskEntity';
import {append, mkdir, readLines, rootPathResolve} from '../utils/file';

const logFolder = Config.get('task.logs');

export abstract class BaseTask<T> extends CoreTask {

    public readonly task: TaskEntity<T>;
    public readonly jobName: string;
    public readonly logFilename: string;

    protected constructor(task: TaskEntity<T>) {
        super();
        this.task = task;

        this.jobName = BaseTask.getJobName(task);
        this.logFilename = `${this.jobName}.log`;
    }

    static getJobName(task: TaskEntity<any>): string {
        return `${task.userId || 'default-user-id'}_${task.key || 'default-key'}`;
    }

    static async readLogs(logFilename: string, limit: number = 100) {
        const file = rootPathResolve(logFolder, logFilename);

        return readLines(file, limit);
    }

    abstract schedule(): void;

    async flog(...args: any) {
        const datetime = moment().format('YYYY-MM-DD HH:ss:mm');

        const lines = _.map(args, (arg) => {
            if (!_.isString(arg)) {
                return `${datetime}: ${JSON.stringify(arg)}`;
            }

            return `${datetime}: ${arg}`;
        }).join('\n');

        const file = rootPathResolve(logFolder, this.logFilename);

        const folder = path.dirname(file);

        await mkdir(folder);

        return append(file, `${fs.existsSync(file) ? '\n' : ''}${lines}`);
    }

}
