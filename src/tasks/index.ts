/**
 * Created by hakan on 04/07/2017.
 */
import * as _ from 'lodash';
import * as schedule from 'node-schedule';

import {Config} from '../../core/src/utils/Config';
import {TaskEntity} from '../entity/TaskEntity';
import {Api} from '../remote/Api';

import {BaseTask} from './BaseTask';
import {UploaderTask} from './UploaderTask';

export default class Index {

    private static tasks: any[] = [];
    private static cron = Config.get('task.schedule');

    public static async tasksStart() {
        try {
            const result: any = await Api.request('/user/tasks');

            // removed task cancel
            for (const task of Index.tasks) {
                const find = _.find(result.items, {userId: task.userId, key: task.key});

                if (!find) {
                    Index.taskCancel(task);
                }
            }

            // start or new task schedule
            for (const task of result.items) {
                const find = _.find<any>(Index.tasks, {userId: task.userId, key: task.key});

                // new task or changed task
                if (!find || !_.isEqual(task, find)) {
                    Index.taskSchedule(task);
                } else {
                    console.log('equal- scheduled task: ', BaseTask.getJobName(task));
                }
            }

            Index.tasks = result.items;
        } catch (e) {

            console.log('/user/tasks request error: ', e);

        }
    }

    public static async taskStart(task: TaskEntity<any>) {
        switch (task.module) {
            case 'uploader':

                const uploader = new UploaderTask(task);
                await uploader.start();
                break;
            case 'timer':
                // const timer = new TimerTask(task);
                // await timer.schedule();
                break;

            default:
        }
    }

    public static async taskSchedule(task: TaskEntity<any>) {
        switch (task.module) {
            case 'uploader':
                const uploader = new UploaderTask(task);
                await uploader.schedule();
                break;

            case 'timer':
                // const timer = new TimerTask(task);
                // await timer.schedule();
                break;

            default:
        }
    }

    public static async taskLogs(task: TaskEntity<any>) {
        switch (task.module) {
            case 'uploader':
                const uploader = new UploaderTask(task);
                return BaseTask.readLogs(uploader.logFilename);
            case 'timer':
                // const timer = new TimerTask(task);
                // await timer.schedule();
                break;

            default:
        }
    }

    public static async taskCancel(task: TaskEntity<any>) {
        switch (task.module) {
            case 'uploader':
                const uploader = new UploaderTask(task);
                await uploader.cancel();
                break;

            case 'timer':
                // const timer = new TimerTask(task);
                // await timer.schedule();
                break;

            default:
        }
    }

    public static async register() {
        const isActive = Config.get('task.active');

        if (!isActive) {
            console.log('task register disable.');
            return;
        }

        console.log('tasksRegister');

        // 0 * * ? * *	Every minute
        // 0 */30 * ? * *	Every 30 minutes
        // 0 0 * ? * *	Every hour
        // 0 0 */6 ? * *	Every six hours
        // 0 0 0 * * ?	Every day at midnight - 12am

        await this.tasksStart();

        schedule.scheduleJob('mp-uploader-task-schedule', Index.cron, this.tasksStart);
    }

}
