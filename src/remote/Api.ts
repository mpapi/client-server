/**
 * Created by hakan on 04/07/2017.
 */

import * as request from 'request';

import {ErrorModel} from '../../core/src/models/ErrorModel';
import {Config} from '../../core/src/utils/Config';

const remoteServer = Config.get('remoteServer');

export class Api {

    private readonly https: boolean;
    private readonly ip: string;
    private readonly port: string;
    private readonly machineKey: string;

    private constructor() {
        this.https = remoteServer.https;
        this.ip = remoteServer.ip;
        this.port = remoteServer.port;
        this.machineKey = remoteServer.machineKey;
    }

    public static request<T>(path: string, data: any = {}, token?: string): Promise<T> {
        return new Api().request(path, data, token);
    }

    public request<T>(path: string, data: any = {}, token?: string): Promise<T> {

        const opts: request.CoreOptions = {
            method: 'POST',
            headers: {
                'User-Agent': 'ApiClient',
                'machine-key': this.machineKey,
            },
            json: data,
        };

        if (token) {
            // @ts-ignore
            opts.headers.token = token;
        }

        return new Promise((resolve, reject) => {
            request.post(this.getUrl() + path, opts, (error, response, body) => {
                if (error) {
                    reject(new ErrorModel('API_CONNECTION_ERROR', 200, error));
                } else if (body.error) {
                    reject(body.error);
                } else {
                    resolve(body);
                }
            });
        });
    }

    private getUrl() {
        return (this.https ? 'https://' : 'http://') + this.ip + (this.port ? ':' + this.port : '');
    }
}
