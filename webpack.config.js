const path = require('path');
const apidoc = require('apidoc');
const webpack = require('webpack');
const nodeExternals = require('webpack-node-externals');
const CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = env => {
    const isDev = env !== 'production';
    return {
        entry: {
            server: './src/index.ts'
        },
        mode: isDev ? 'development' : 'production',
        output: {
            filename: 'index.js',
            path: path.resolve(__dirname, 'dist')
        },
        resolve: {
            modules: ['src', 'node_modules'],
            extensions: ['.ts', '.json']
        },
        module: {
            rules: [
                {
                    test: /.ts?$/,
                    use: [
                        'ts-loader'
                    ]
                }
            ]
        },
        target: 'node',
        externals: [
            nodeExternals(),
            (context, request, callback) => {
                if (/config\.json$/.test(request)) {
                    return callback(null, 'commonjs ./config.json');
                }
                callback();
            }
        ],
        plugins: [
            new webpack.EnvironmentPlugin({
                dev: isDev
            }),
            new CopyWebpackPlugin([
                {
                    from: 'src/config.json',
                    to: 'config.json',
                    transform(content) {
                        const sub = isDev ? 'development' : 'production';
                        const json = JSON.parse(content);
                        return JSON.stringify(json[sub], null, 2);
                    }
                },
                {
                    from: 'package.json',
                    to: 'package.json',
                    transform(content) {
                        const json = JSON.parse(content);

                        json.scripts = {"start": "node index.js"};
                        delete json.devDependencies;

                        return JSON.stringify(json, null, 2);
                    }
                },
            ]),
            () => {
                return apidoc.createDoc({
                    src: 'src',
                    dest: 'dist/public/docs',
                    silent: true
                });
            }
        ],
        watch: isDev
    };
};
